using System;
using System.Text;
using System.Threading.Tasks;
using WindesheimAD2021AutoVerzekeringsPremie;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremieTest
{
    public class PremiumCalculationTest
    {
        // This program checks whether or not the input for the UpdatePremiumForPostalCode is correct.
        [Theory]
        [InlineData(1000, 4, 3600  )]
        [InlineData(double.MinValue, 1000, double.MaxValue)]
        public void CheckPremiumForPostalCode(double premium, int postalCode, double expected) 
        {
            // Arrange

            // Act
            double actual = PremiumCalculation.UpdatePremiumForPostalCode(premium, postalCode);

            // Assert
            Assert.Equal(expected, actual);
        }

        // This program checks whether or not the input for the UpdatePremiumForNoClaimYears is correct.
        [Theory]
        [InlineData(1, 2, 3600)]
        [InlineData(double.MinValue, 1000, double.MaxValue)]
        public void CheckPremiumForNoClaimYears(double premium, int years, double expected)
        {
            // Arrange

            // Act
            double actual = PremiumCalculation.UpdatePremiumForNoClaimYears(premium, years);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
