using System;
using System.Text;
using System.Threading.Tasks;
using WindesheimAD2021AutoVerzekeringsPremie;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremieTest
{
    public class ProgramTest
    {
      
        //This test checks if PolicyHolder.cs can see the current date. In this test the value of the current date will turn into zero. If the value of the date isn't zero, then the test have been given the wrong date.
        [Fact]
        public void CheckIfCurrentDateIsCorrect()
        {
            // Arrange
            int expectedOutcome = 0;
            
            // Act
            int actualOutcome = PolicyHolder.AgeByDate(DateTime.Now);

            // Assert
            Assert.Equal(expectedOutcome, actualOutcome);
        }

        //This test checks if string for the date can be parsed.
        [Fact]
        public void CheckIfStringCanBeParsed()
        {
            // Arrange
            int expectedOutcome = 0;

            // Act
            int actualOutcome = PolicyHolder.ParseDate(DateTime.Now);

            // Assert
            Assert.Equal(expectedOutcome, actualOutcome);
        }
    }
}
